# frozen_string_literal: true

# Add new table for cities
class AddModelCity < ActiveRecord::Migration[6.1]
  def change
    create_table :cities do |t|
      t.string :name
      t.string :postal_code

      t.timestamps
    end
  end
end
