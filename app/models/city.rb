# frozen_string_literal: true

# City with name and zip code, that hosts transactions
class City < ApplicationRecord
  has_many :transactions

  BASE_URL = 'https://data.opendatasoft.com/api/records/1.0/search/?dataset=demande-de-valeurs-foncieres-agrege-a-la-transaction%40public&q='

  def fetch_transactions!
    url = "#{BASE_URL}&facet=code_postal&refine.code_postal=#{postal_code}"
    records = JSON.parse(Typhoeus.get(url).response_body)['records']

    records.each do |record|
      update(name: record['fields']['commune']) unless name
      tr = transactions.find_or_create_by(record_id: record['recordid'])
      tr.update_from_record(record)
    end
  end
end
