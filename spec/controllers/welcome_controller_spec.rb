# frozen_string_literal: true

require 'rails_helper'

RSpec.describe WelcomeController, type: :controller do
  render_views

  describe 'GET index' do
    subject { get :index }

    it { is_expected.to have_http_status(:ok) }

    it 'does not display any city' do
      subject
      expect(response.body.squish).to include(
        '<h3>Villes synchronisées :</h3> ' \
        '<table border=1> <tr> ' \
        '<th>Code postal</th> <th>Nom</th> ' \
        '</tr> </table>'
      )
    end

    context 'with cities' do
      before { create(:city) }

      it 'displays a list of cities' do
        subject
        expect(response.body.squish).to include(
          '<h3>Villes synchronisées :</h3> ' \
          '<table border=1> <tr> ' \
          '<th>Code postal</th> <th>Nom</th> </tr> ' \
          '<tr> <td><a href="/transactions?zip_code=59110">59110</a></td> ' \
          '<td><a href="/transactions?zip_code=59110">La Madeleine</a></td> ' \
          '</tr> </table>'
        )
      end
    end
  end
end
